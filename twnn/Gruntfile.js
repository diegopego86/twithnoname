module.exports = function(grunt) {
  grunt.initConfig({
    compass: {
      dist: {
        options: {
          sassDir: 'sass',
          cssDir: 'css',
          environment: 'production',
          noLineComments: false
        }
      }
    },
    watch: {
      styles: {
        files: ['**/*.scss'], 
        tasks: ['compass','replace'],
      },
      script: {
        files: ['js/*','!js/*.min.js'],
        tasks: ['concat','uglify'],
      }
    },
    replace:{
      dist:{
        src: ['css/style.css'],             // source files array (supports minimatch)
        overwrite: true,
        replacements: [{
          from: /;(\r\n|\n)\/\*n/g,      // regex replacement ('Fooo' to 'Mooo')
          to: ''
        },{
          from: '=*/',                   // string replacement
          to: '%%*=/'
        },{
          from: '}*/',                   // string replacement
          to: '}*/;'
        },{
          from: '%%*=/',                   // string replacement
          to: '*/'
        },{
          from: '  ',                   // string replacement
          to: ' '
        }]
      }
    },
    uglify: {
      my_target: {
        options: {
          preserveComments: 'some'
        },
        files: [{
          expand: true,
          cwd: 'js',
          src: ['**/*.js','!**/*.min.js'],
          dest:'js',
          ext: '.min.js'
        }]
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: ['bower_components/owl.carousel/dist/owl.carousel.js','js/spectragramx.js', 'js/custom.js'],
        dest: 'js/twnn.js',
      },
    }
  });
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-text-replace');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['watch']);
};