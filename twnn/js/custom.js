// NOTICE!! THIS IS REQUIRED TO MAKE YOUR NETO SHOPPING CART WORK
// DO NOT REMOVE UNLESS YOU REALLY KNOW WHAT YOU ARE DOING
var Mobile = {
    Windows: function() {
        return /IEMobile/i.test(navigator.userAgent);
    },
    Android: function() {
        return /Android/i.test(navigator.userAgent);
    },
    BlackBerry: function() {
        return /BlackBerry/i.test(navigator.userAgent);
    },
    iOS: function() {
        return /iPhone|iPad|iPod/i.test(navigator.userAgent);
    },
    any: function() {
        return (Mobile.Android() || Mobile.BlackBerry() || Mobile.iOS() || Mobile.Windows());
    }
};
var isMobile = Mobile.any();
(function($) {
	$.fn.spectragramx.accessData = {
		accessToken: '1574195005.a074e59.ee2401d4f3054006adc4bed1201b53ba'
	};
	$.extend({
		initPageFuncs: function() {
			// Ajax Wish List
			$.addToWishList({
				'class': 'wishlist_toggle',
				'textclass': 'wishlist_text',
				'htmlon': 'Remove From Wishlist',
				'htmloff': 'Add To Wishlist',
				'tooltip_css': 'whltooltips'
			});
			// Ajax Add To Cart
			$.addToCartInit({
				'cart_id' :  'cartcontents',
				'target_id': 'cartcontentsheader',
				'image_rel': 'itmimg'
			});

			$(".disp_ajax_templ").unbind();
			$(".disp_ajax_templ").change(function() {
				var sku = $(this).val();
				var rel = $(this).attr('rel');
				$.load_ajax_template(rel, {'sku':sku, 'showloading':true, 'procdata':'n'}, {onLoad: function (){$.initPageFuncs();}});
			});
			// This renders the instant search results - edit design of ajax results here
			$.initSearchField({
				'result_header'		: '<ul class="nav nav-list">',
				'result_body'		: '<li><a href="javascript:void(0);" search-keyword="##keyword##"><img border="0" src="##thumb##" width="36" height="36"/><span class="title">##model##</span></a></li>',
				'result_footer'		: '</ul>',
				'category_header'	: '<ul class="nav nav-list">',
				'category_body'		: '<li><a href="##url##"><span class="thumb"><img border="0" src="##thumb##" width="36" height="36"/></span><span class="title">##fullname##</span> <span class="label label-default">##typename##</span></a></li>',
				'category_footer'	: '</ul>'
			});
		},

// For child product multi-add to cart function
		checkValidQty: function() {
			var found = 0;
			$("#multiitemadd :input").each(function() {
				if ($(this).attr('id').match(/^qty/)) {
					if ($(this).val() > 0) {
						found = 1;
					}
				}
			});
			if (found == 0) {
				$.fancybox("Please specify a quantity before adding to cart");
				return false;
			}
			return true;
		},

		modQtyByMulti: function(obj,act) {
			var mul = 1;
			var maxm;
			var minm = 0;
			var objid = obj.replace(/^qty/,'');
			if ($('#qty'+objid).length > 0) {
				if ($('#multiplier_qty'+objid).length > 0) {
					mul = $('#multiplier_qty'+objid).val();
				}
				if ($('#min_qty'+objid).length > 0) {
					minm = $('#min_qty'+objid).val();
				}
				if ($('#max_qty'+objid).length > 0) {
					maxm = $('#max_qty'+objid).val();
				}

				var cur = $('#'+obj).val();
				if (isNaN(cur)) {
					cur = 0;
				}

				if (act == 'add') {
					cur = parseInt(cur) + parseInt(mul);
					if (!isNaN(maxm) && cur > maxm) {
						cur = maxm;
					}
				}
				else if (act == 'subtract') {
					cur = parseInt(cur) - parseInt(mul);
					if (cur < minm) {
						cur = minm;
					}
				}

				$('#qty'+objid).val(cur);
			}
		}
	});	
	function dynamicSort(property) {
		var sortOrder = 1;
		if(property[0] === "-") {
			sortOrder = -1;
			property = property.substr(1);
		}
		return function (a,b) {
			var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
			return result * sortOrder;
		}
	}
	if ($('.homepage--instagram').length > 0){
		$.spectragramx('getRawData').done(function(results){
			var posts = results.data;
			var total_posts = 4;
			var posts_to_show = [];
			var posts_to_show_ids = [];
			var html_results = '';
			for(var p=0; p < posts.length; p++){
				var tags = posts[p].tags;
				if ($.inArray('twnn', tags) > -1){
					posts_to_show.push(posts[p]);
					posts_to_show_ids.push(posts[p].id);
				}
				if (posts_to_show.length >= total_posts){
					break;
				}
			}
			if (posts_to_show.length < total_posts){
				for(var p = 0; p < posts.length; p++){
					if($.inArray(posts[p].id,posts_to_show_ids) < 0){
						posts_to_show.push(posts[p]);
						posts_to_show_ids.push(posts[p].id);
					}
					if (posts_to_show.length >= total_posts){
						break;
					}
				}
			}
			posts_to_show.sort(dynamicSort('-created_time'));
			for ( var i = 0; i < posts_to_show.length; i++ ) {
				var imageURL = posts_to_show[i].images.standard_resolution.url;
				var imageCaption = ( posts_to_show[i].caption !== null ) ?
										$( "<span>" ).text( posts_to_show[i].caption.text ).html() :
										'Instagram image';
				var link = posts_to_show[i].link;
				html_results+= '<li class="instagram--item "><a href="'+link+'" target="_blank" title="'+imageCaption+'">'
					+'<img src="'+imageURL+'" alt="'+imageCaption+'"/>'
					+'</a></li>'
			}
			$('.homepage--instagram .instragram--list').html(html_results);
		});
	}
})(jQuery);

$(document).ready(function() {
	// Popup Credit Card CCV Description At Checkout
	$("#card_ccv").fancybox();
	if(isMobile){
		$('body').addClass('mobile');
	}else{
		$('body').addClass('desktop');
	}
	// Popup Terms At Checkout
	$("#terms").fancybox({
		'width' : 850,
		'height': 650
	});

	// Jquery Ui Date Picker
	$(".datepicker").datepicker({ dateFormat: "dd/mm/yy" });
	$.initPageFuncs();
	if($('.custom-dye-swatches-slider').length > 0){
		$('.custom-dye-swatches-slider').owlCarousel({
		    loop:false,
		    margin:50,
		    nav: false,
		    dots: false,
		    items: 6,
		    stagePadding: 50,
		    navText: ['<button type="button" class="button-prev"><svg viewBox="0 0 26 46"><title>arrow-left</title> <g> <path d="M24.4,0.6C24.1,0.2,23.5,0,23,0c-0.5,0-1,0.2-1.4,0.6l-21,21c-0.8,0.8-0.8,2.1,0,2.8l21,21c0.8,0.8,2.1,0.8,2.8,0c0.8-0.8,0.8-2.1,0-2.8L4.9,23L24.4,3.4C25.2,2.6,25.2,1.4,24.4,0.6z"/> </g> </svg></button>',
		    '<button type="button" class="button-next"><svg viewBox="0 0 26 46"><title>arrow-right</title> <g> <path d="M1.6,45.4C2,45.8,2.5,46,3,46c0.5,0,1-0.2,1.4-0.6l21-21c0.8-0.8,0.8-2.1,0-2.8l-21-21c-0.8-0.8-2.1-0.8-2.8,0c-0.8,0.8-0.8,2.1,0,2.8L21.2,23L1.6,42.6C0.8,43.4,0.8,44.7,1.6,45.4z"/> </g> </svg></button>'],
		    responsive:{
		        0:{
		            dots: false,
		            nav: true,
		            items: 2
		        },
		        768:{
		            dots: false,
		            nav: true,
		            items: 4
		        },
		        992:{
		            dots: false,
		            nav: false,
		            items: 6
		        }
		    }
		});
	}	
	$('button[name=change-postcode]').on('click',function(){
		$('.postcode-form').toggleClass('open');
	});
	$('.slider').owlCarousel({
	    loop:true,
	    autoplay: true,
	    autoplayTimeout: 5000,
	    margin:10,
	    nav: true,
	    dots: false,
	    items: 1,
	    navText: ['<button type="button" class="button-prev"><svg viewBox="0 0 26 46"><title>arrow-left</title> <g> <path d="M24.4,0.6C24.1,0.2,23.5,0,23,0c-0.5,0-1,0.2-1.4,0.6l-21,21c-0.8,0.8-0.8,2.1,0,2.8l21,21c0.8,0.8,2.1,0.8,2.8,0c0.8-0.8,0.8-2.1,0-2.8L4.9,23L24.4,3.4C25.2,2.6,25.2,1.4,24.4,0.6z"/> </g> </svg></button>',
	    '<button type="button" class="button-next"><svg viewBox="0 0 26 46"><title>arrow-right</title> <g> <path d="M1.6,45.4C2,45.8,2.5,46,3,46c0.5,0,1-0.2,1.4-0.6l21-21c0.8-0.8,0.8-2.1,0-2.8l-21-21c-0.8-0.8-2.1-0.8-2.8,0c-0.8,0.8-0.8,2.1,0,2.8L21.2,23L1.6,42.6C0.8,43.4,0.8,44.7,1.6,45.4z"/> </g> </svg></button>'],
	    responsive:{
	        0:{
	            dots: false,
	            nav: true,
	            autoplay: false,
	            autoHeight: true
	        },
	        768:{
	            dots: false,
	            nav: true,
	            autoplay: true,
	            autoHeight: true
	        },
	        992: {
	        	dots: false,
	        	nav: true,
	        	autoplay: true,
	        	autoHeight: true
	        }
	    }
	});
	$('.page-slider').owlCarousel({
	    loop:true,
	    autoplay: true,
	    autoplayTimeout: 5000,
	    margin:10,
	    nav: true,
	    dots: false,
	    items: 1,
	    navText: ['<button type="button" class="button-prev"><svg viewBox="0 0 26 46"><title>arrow-left</title> <g> <path d="M24.4,0.6C24.1,0.2,23.5,0,23,0c-0.5,0-1,0.2-1.4,0.6l-21,21c-0.8,0.8-0.8,2.1,0,2.8l21,21c0.8,0.8,2.1,0.8,2.8,0c0.8-0.8,0.8-2.1,0-2.8L4.9,23L24.4,3.4C25.2,2.6,25.2,1.4,24.4,0.6z"/> </g> </svg></button>',
	    '<button type="button" class="button-next"><svg viewBox="0 0 26 46"><title>arrow-right</title> <g> <path d="M1.6,45.4C2,45.8,2.5,46,3,46c0.5,0,1-0.2,1.4-0.6l21-21c0.8-0.8,0.8-2.1,0-2.8l-21-21c-0.8-0.8-2.1-0.8-2.8,0c-0.8,0.8-0.8,2.1,0,2.8L21.2,23L1.6,42.6C0.8,43.4,0.8,44.7,1.6,45.4z"/> </g> </svg></button>']	   
	});
	$('.filter-button').on('click', function(){
		$('#left-sidebar').addClass('active');
	});
	$('#left-sidebar .btn-close').on('click', function(){
		$('#left-sidebar').removeClass('active');
	});
	$('.mobile-menu-button').on('click', function(){
		if($(this).hasClass('open')){
			$('body').removeClass('menu-open');
			$(this).removeClass('open');
		}else{
			$('body').addClass('menu-open');
			$(this).addClass('open');
		}
	});
	if($('#filters').length>0){
		$('#filters').addClass('ready');
		$('#filters ul:not(.open)').slideUp(0);
		$('#filters h4').on('click', function(){
			$(this).toggleClass('open');
			if($(this).hasClass('open')){
				$(this).next().addClass('open').slideDown(300);
				$(this).find('span').text('-');
			}else{
				$(this).next().removeClass('open').slideUp(300);
				$(this).find('span').text('+');
			}
		});
	}
	if($('.sort-by').length > 0){
		$('.sort-by').on('click', function(event){
			$(this).toggleClass('active');
			event.stopPropagation();
		});
		$('.sort-by li').on('click', function(event){
			$('select[name=sortby]').val($(this).data('value'));
			$('select[name=sortby]').trigger('change');
		});
		$(document).on('click', function(elem){
			$('.sort-by').removeClass('active');
		});
	}
	
});

$(".btn-loads").click(function(){
	$(this).button("loading");
	var pendingbutton=this;
	setTimeout(function(){
		$(pendingbutton).button("reset");
	},3000);
});

// Fancybox
$(document).ready(function() {
	//$(".fancybox").fancybox();
});

// Tooltip
//$('.tipsy').tooltip({trigger:'hover',placement:'bottom'});

// Who needs AddThis?
function windowPopup(url, width, height) {
	// Calculate the position of the popup so
	// it’s centered on the screen.
	var left = (screen.width / 2) - (width / 2),
		top = (screen.height / 2) - (height / 2);
	window.open(url,"","menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=" + width + ",height=" + height + ",top=" + top + ",left=" + left);
}
$(".js-social-share").on("click", function(e) {
	e.preventDefault();
	windowPopup($(this).attr("href"), 500, 300);
});

$('.nToggleMenu').click(function(){
	var toggleTarget = $(this).attr('data-target')
	$(toggleTarget).slideToggle();
});
